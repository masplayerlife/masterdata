/**
 * 
 */
package mx.isban.multivendor.si.importmasterdata.controller;

import javax.ejb.EJB;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.multivendor.si.general.constantes.MultivendorConstantes;
import mx.isban.multivendor.si.masterdata.beans.RequestMasterDataDTO;
import mx.isban.multivendor.si.masterdata.beans.ResponseMasterDataDTO;
import mx.isban.multivendor.si.masterdata.ejb.BOMasterData;

/** 
* Isban Mexico
* Clase: ImportMasterDataRestService.java
* Descripcion: Servicio recibe peticion de 
* importar master data a BD TM Server.
*
* Control de Cambios:
* 1.0 30/05/2018 José Manuel Almanza Sancén - Creación
*/
@RestController
@RequestMapping(value = MultivendorConstantes.REQ_MASTERDATA, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ImportMasterDataRestService extends Architech {

	/**
	 * ID.
	 */
	private static final long serialVersionUID = 1512576748320671729L;
	/**
	 * BO
	 */
	@EJB
	private BOMasterData boMasterData;
	/**
	 * Función para importar master data
	 * @param request
	 * @return
	 * 
	 */
	@RequestMapping(value = MultivendorConstantes.REQ_MASTERDATA_IMPORTAR, method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResponseMasterDataDTO> importarMasterData(@RequestBody @Valid RequestMasterDataDTO request) {
		this.info("EMPIEZA IMPORTAR MASTER DATA SERVICE");
		//Objeto de respuesta del servicio
		ResponseMasterDataDTO response = new ResponseMasterDataDTO();
		HttpStatus httpStatus = HttpStatus.CREATED;
		try {
			//se envia el request para crear XML con la data del cajero			
			response = boMasterData.importarMasterData(request, this.getArchitechBean());
		} catch (BusinessException e) {
			//Log error
			this.error(e.getMessage(), e);
			//se asigna código y mensaje de error
			response.setCodeError(e.getCode());
			response.setMessageError(e.getMessage());
		}
		this.info("SALIENDO DE IMPORTAR MASTER DATA REST SERVICE");		
		//regresamos respuesta del servicio al consumer
		return ResponseEntity.status(httpStatus).body(response);
	}
}