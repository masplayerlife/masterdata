package mx.isban.multivendor.si.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.multivendor.si.error.ErrorCode;
import mx.isban.multivendor.si.masterdata.beans.ResponseMasterDataDTO;

/**
 * 
 * Handler general para el manejo de las exception
 * @author 01/06/2018 JMAS - Creacion
 *
 */
@ControllerAdvice
public class RestErrorHandler extends Architech {

	/**
	 * Serializable ID
	 */
	private static final long serialVersionUID = 825134110977120921L;
	/**
	 * ExceptionHandler para las exceptions especificadas ConstraintViolationException 
	 * @param exception
	 * @return ResponseMasterDataDTO
	 */
	@ExceptionHandler
	@ResponseBody
	public ResponseEntity<ResponseMasterDataDTO> handleValidationException(MethodArgumentNotValidException exception) {
		error("INICIA HANDLE ERROR");
		//valores de respuestas en la exception
		ResponseMasterDataDTO responseBean = new ResponseMasterDataDTO();
		//Mensaje de error en la validacion del request
		String message = exception.getBindingResult().getFieldError().getDefaultMessage();
		//Set de codigo error y mensaje
		responseBean.setCodeError(ErrorCode.COD_ERR_REQUIRED);
		responseBean.setMessageError(message);
		//Se regresa respuesta
		return new ResponseEntity<ResponseMasterDataDTO> (responseBean, HttpStatus.BAD_REQUEST);
	}
}
