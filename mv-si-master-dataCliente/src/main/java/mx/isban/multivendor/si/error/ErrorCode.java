package mx.isban.multivendor.si.error;

/**
 * Clase para la respuesta y codigo de error para las peticiones 
 * al servicio 
 * @author 01/06/2018 JMAS
 *
 */
public final class ErrorCode {
	/**
	 * CODIGO DE ERROR PARAMETROS REQUERIDOS
	 */
	public static final String COD_ERR_REQUIRED = "MVSI007";
	/**
	 * Constructor privado
	 */
	private ErrorCode() {
		
	}

}
