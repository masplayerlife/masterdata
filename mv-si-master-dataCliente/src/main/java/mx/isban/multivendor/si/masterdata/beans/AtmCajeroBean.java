package mx.isban.multivendor.si.masterdata.beans;

import java.io.Serializable;

/**
 * Queretaro, Mayo 2018 Clase AtmCajeroBean
 * 
 * Bean con datos para crear XML
 * 
 * @author JMAS
 * @version 1.0
 *
 */
public class AtmCajeroBean implements Serializable {
	
	/**
	 * ID.
	 */
	private static final long serialVersionUID = -7611123642700750709L;
	/**
	 * numCajero para XML
	 */
	private String numCajero;
	/**
	 * nombre para XML
	 */
	private String nombre;
	/**
	 * folio para XML
	 */
	private String folio;
	/**
	 * calle para XML
	 */
	private String calle;
	/**
	 * num para XML
	 */
	private String num;
	/**
	 * colonia para XML
	 */
	private String colonia;
	/**
	 * delegacion para XML
	 */
	private String delegacion;
	/**
	 * ciudad para XML
	 */
	private String ciudad;
	/**
	 * estado para XML
	 */
	private String estado;
	/**
	 * cp para XML
	 */
	private String cp;
	/**
	 * num serie para XML
	 */
	private String numSerie;
	/**
	 * modelo para XML
	 */
	private String modelo;
	/**
	 * marca para XML
	 */
	private String marca;
	/**
	 * serire para XML
	 */
	private String serie;
	/**
	 * estatus para XML
	 */
	private String estatus;
	/**
	 * observaciones para XML
	 */
	private String observaciones;
	/**
	 * num cajero
	 * @return
	 */
	public String getNumCajero() {
		return numCajero;
	}
	/**
	 * numCajero to set
	 * @param numCajero
	 */
	public void setNumCajero(String numCajero) {
		this.numCajero = numCajero;
	}
	/**
	 * nombre
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * nombre to set
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * folio
	 * @return
	 */
	public String getFolio() {
		return folio;
	}
	/**
	 * folio to set
	 * @param folio
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
	/**
	 * calle
	 * @return
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * calle to set
	 * @param calle
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * num 
	 * @return
	 */
	public String getNum() {
		return num;
	}
	/**
	 * num to set
	 * @param num
	 */
	public void setNum(String num) {
		this.num = num;
	}
	/**
	 * colonia
	 * @return
	 */
	public String getColonia() {
		return colonia;
	}
	/**
	 * colonia to set
	 * @param colonia
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	/**
	 * delegacion
	 * @return
	 */
	public String getDelegacion() {
		return delegacion;
	}
	/**
	 * delegacion to set
	 * @param delegacion
	 */
	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}
	/**
	 * ciudad
	 * @return
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * ciudad to set
	 * @param ciudad
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * estado
	 * @return
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * estado to set
	 * @param estado
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * cp
	 * @return
	 */
	public String getCp() {
		return cp;
	}
	/**
	 * cp to set
	 * @param cp
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}
	/**
	 * num serie
	 * @return
	 */
	public String getNumSerie() {
		return numSerie;
	}
	/**
	 * numserie to set
	 * @param numSerie
	 */
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	/**
	 * modelo
	 * @return
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * modelo to set
	 * @param modelo
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * marca 
	 * @return
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * marca to set
	 * @param marca
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * serie
	 * @return
	 */
	public String getSerie() {
		return serie;
	}
	/**
	 * serie to set
	 * @param serie
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}
	/**
	 * estatus
	 * @return
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * estatus to set
	 * @param estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * observaciones
	 * @return
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/**
	 * observaciones to set
	 * @param observaciones
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/**
	 * imprimer bean
	 */
	@Override
	public String toString() {
		return "AtmCajeroBean [numCajero=" + numCajero + ", nombre=" + nombre + ", folio=" + folio + ", calle=" + calle
				+ ", num=" + num + ", colonia=" + colonia + ", delegacion=" + delegacion + ", ciudad=" + ciudad
				+ ", estado=" + estado + ", cp=" + cp + ", numSerie=" + numSerie + ", modelo=" + modelo + ", marca="
				+ marca + ", serie=" + serie + ", estatus=" + estatus + ", observaciones=" + observaciones + "]";
	}
	
}
