package mx.isban.multivendor.si.masterdata.beans;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Queretaro, Mayo 2018
 * 
 * Clase con request de datos para servicio de 
 * importar masterdata
 *  
 * @author JMAS
 * @version 1.0
 *
 */
public class RequestMasterDataDTO implements Serializable {
	/**
	 * ID.
	 */
	private static final long serialVersionUID = -6551930609175063379L;
	/**
	 * Tipo de Manejo de error (SKIP, ABORT, ROLLBACK)
	 */
	@NotNull(message="El tipo de manejo de error es requerido.")	
	private String tipoManejoError;
	/**
	 * numero de cajero
	 */
	@NotNull(message="Campo numero cajero requerido.")
	@Size(min=6, max=6, message="Longitud caracteres de numero cajero debe ser 6.")
	private String numCajero;
	/**
	 * crear entitidades 
	 */
	private boolean createEntities;
	/**
	 * actualizar entidades
	 */
	private boolean updateEntities;	
	/**
	 * manejo de error
	 * @return
	 */
	public String getTipoManejoError() {
		return tipoManejoError;
	}
	/**
	 * manejo de error to set
	 * @param tipoManejoError
	 */
	public void setTipoManejoError(String tipoManejoError) {
		this.tipoManejoError = tipoManejoError;
	}
	/**
	 * numCajero
	 * @return
	 */
	public String getNumCajero() {
		return numCajero;
	}
	/**
	 * numCajero to set
	 * @param numCajero
	 */
	public void setNumCajero(String numCajero) {
		this.numCajero = numCajero;
	}
	/**
	 * crear entidades
	 * @return
	 */
	public boolean isCreateEntities() {
		return createEntities;
	}
	/**
	 * crear entidades to set
	 * @param createEntities
	 */
	public void setCreateEntities(boolean createEntities) {
		this.createEntities = createEntities;
	}
	/**
	 * actualizar entidades
	 * @return
	 */
	public boolean isUpdateEntities() {
		return updateEntities;
	}
	/**
	 * actualizar entidades to set
	 * @param updateEntitites
	 */
	public void setUpdateEntities(boolean updateEntities) {
		this.updateEntities = updateEntities;
	}
}