package mx.isban.multivendor.si.masterdata.ejb;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.multivendor.si.masterdata.beans.AtmCajeroBean;
import mx.isban.multivendor.si.masterdata.beans.RequestMasterDataDTO;
import mx.isban.multivendor.si.masterdata.beans.ResponseMasterDataDTO;

/**
 * Queretaro, Mayo 2018 
 * BOMasterData
 * 
 * Interfaz que define metodo para importar
 * master data y metodo para crear XML
 * 
 * @author JMAS
 *
 */
public interface BOMasterData {
	/**
	 * Función que envia el request para crear XML
	 * @param request
	 * @param customers
	 * @param asb
	 * @return
	 * @throws BusinessException
	 */
	ResponseMasterDataDTO importarMasterData(RequestMasterDataDTO request, ArchitechSessionBean asb) throws BusinessException;
	/**
	 * Función para crear XML
	 * @param atm
	 * @param request
	 * @param customers
	 * @param asb
	 * @return
	 * @throws BusinessException
	 */
	String crearXML(AtmCajeroBean atm, RequestMasterDataDTO request, ArchitechSessionBean asb) throws BusinessException;
}
