package mx.isban.multivendor.si.general.constantes;

import mx.isban.multivendor.si.utils.MultivendorUtil;

/**
 * Querétaro, Mayo 2018
 * Clase MultivendorConstantes
 * Clase para constantes que se usaran en el aplicativo 
 * @author JMAS
 */
public final class MultivendorConstantes {
	/**
	 * RequestMapping controller masterdata
	 */
	public static final String REQ_MASTERDATA = "/masterdata";
	/**
	 * RequestMapping metodo importar
	 */
	public static final String REQ_MASTERDATA_IMPORTAR = "/importar";	
	/**
	 * Propiedad con el nombre de la ruta donde se encuentra el properties
	 */
	public static final String PROP_RUTA_PROPERTIES_ERRORES = "RUTA_PROPERTIES_ERRORES";
	/**	
	 * USUARIO OCM
	 */
	public static final String USER = MultivendorUtil.getInstance().getFieldProperties("USER");
	/**
	 * PASSWORD OCM
	 */
	public static final String PASSWORD = MultivendorUtil.getInstance().getFieldProperties("PASSWORD");
	/**	
	 * CODIGO DE OPERACION EXITOSA
	 */
	public static final String COD_ERR_EXITO = MultivendorUtil.getInstance().getFieldProperties("COD_ERR_EXITO");
	/**
	 * MENSAJE DE OPERACION EXITOSA
	 */
	public static final String MSG_ERR_EXITO = MultivendorUtil.getInstance().getFieldProperties("MSG_ERR_EXITO");	
	/**
	 * CODIGO DE ERROR NO SE ENCONTRO CAJERO
	 */
	public static final String COD_ERR_NOT_FOUND = MultivendorUtil.getInstance().getFieldProperties("COD_ERR_NOT_FOUND");
	/**
	 * MENSAJE DE ERROR NO SE ENCONTRO CAJERO
	 */
	public static final String MSG_ERR_NOT_FOUND = MultivendorUtil.getInstance().getFieldProperties("MSG_ERR_NOT_FOUND");
	/**
	 * CODIGO DE ERROR AL ENVIAR XML
	 */
	public static final String COD_ERR_ENVIAR_XML = MultivendorUtil.getInstance().getFieldProperties("COD_ERR_ENVIAR_XML");
	/**
	 * MENSAJE DE ERROR AL ENVIAR XML
	 */
	public static final String MSG_ERR_ENVIAR_XML = MultivendorUtil.getInstance().getFieldProperties("MSG_ERR_ENVIAR_XML");
	/**
	 * TAGS ARCHIVO XML DATACENTER
	 */
	//DATACENTER NAME
	public static final String DATACENTER_NAME = MultivendorUtil.getInstance().getFieldProperties("DATACENTER_NAME");
	//DATACENTER DESCRIPTION
	public static final String DATACENTER_DESCRIPTION = MultivendorUtil.getInstance().getFieldProperties("DATACENTER_DESCRIPTION");
	//DATACENTER STREET
	public static final String DATACENTER_STREET = MultivendorUtil.getInstance().getFieldProperties("DATACENTER_STREET");
	//DATACENTER
	public static final String DATACENTER_CITY = MultivendorUtil.getInstance().getFieldProperties("DATACENTER_CITY");
	/**
	 * TAGS ARCHIVO XML MANDATOR
	 */
	//MANDATOR NAME
	public static final String MANDATOR_NAME = MultivendorUtil.getInstance().getFieldProperties("MANDATOR_NAME");
	//MANDATOR DESCRIPTION
	public static final String MANDATOR_DESCRIPTION = MultivendorUtil.getInstance().getFieldProperties("MANDATOR_DESCRIPTION");
	/**
	 * TAGS ARCHIVO XML INSTITUTE
	 */
	//INSTITUTE NAME
	public static final String INSTITUTE_NAME = MultivendorUtil.getInstance().getFieldProperties("INSTITUTE_NAME");
	//INSTITUTE DESCRIPTION
	public static final String INSTITUTE_DESCRIPTION = MultivendorUtil.getInstance().getFieldProperties("INSTITUTE_DESCRIPTION");
	//INSTITUTE ROUTING CODE
	public static final String INSTITUTE_ROUTING_CODE = MultivendorUtil.getInstance().getFieldProperties("INSTITUTE_ROUTING_CODE");
	//INSTITUTE SHORT ROUTING CODE
	public static final String INSTITUTE_SHORT_ROUTING_CODE = MultivendorUtil.getInstance().getFieldProperties("INSTITUTE_SHORT_ROUTING_CODE");
	/**
	 * TAGS ARCHIVO XML SUBSIDIARY
	 */
	//SUBSIDIARY NAME
	public static final String SUBSIDIARY_NAME = MultivendorUtil.getInstance().getFieldProperties("SUBSIDIARY_NAME");
	//SUBSIDIARY DESCRIPTION
	public static final String SUBSIDIARY_DESCRIPTION = MultivendorUtil.getInstance().getFieldProperties("SUBSIDIARY_DESCRIPTION");
	/**
	 * TAGS ARCHIVO XML CUSTOMER
	 */
	public static final String GROUP_ID = MultivendorUtil.getInstance().getFieldProperties("GROUP_ID");
	/**
	 * CONSTRUCTOR PRIVADO
	 */
	private MultivendorConstantes(){
		//Private constructor
	}
}









