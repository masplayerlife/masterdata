package mx.isban.multivendor.si.masterdata.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import mx.isban.agave.dataaccess.ResponseMessageDTO;
/**
 * Clase para las respuestas del servicio importMasterData
 * @author JMAS
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMasterDataDTO extends ResponseMessageDTO implements Serializable {	
	/**
	 * ID.
	 */
	private static final long serialVersionUID = -8826388842123285520L;
	/**	
	 * Conjunto de todas las entidades creadas por esta importación
	 */
	private List<String> createdEntities;
	/**
	 * Conjunto de todas las entidades subidas por esta importación
	 */
	private List<String> updatedEntities;
	/**
	 * Conjunto de todas las entidades reubicadas por esta importación 
	 */
	private List<String> movedEntities;
	/**
	 * Conjunto de todas las entidades con excepciones en esta importación
	 */
	private List<String> exceptionEntities;
	/**
	 * Entidades creadas to get
	 * Return a copy
	 * @return
	 */
	public List<String> getCreatedEntities() {
		List<String> response = new ArrayList<String>();
		if (this.createdEntities != null && !this.createdEntities.isEmpty()) {
			for (String object : this.createdEntities) {
				response.add(object);
			}
		}
		return response;
	}
	/**
	 * Entidades creadas to set
	 * Set a copy
	 * @param createdEntities
	 */
	public void setCreatedEntities(List<String> createdEntities) {
		List<String> response = new ArrayList<String>();
		if (createdEntities != null && !createdEntities.isEmpty()) {
			for (String object : createdEntities) {
				response.add(object);
			}
		}
		this.createdEntities = response;
	}
	/**
	 * Entidades actualizadas to get
	 * Return a copy
	 * @return
	 */
	public List<String> getUpdatedEntities() {
		List<String> response = new ArrayList<String>();
		if (this.updatedEntities != null && !this.updatedEntities.isEmpty()) {
			for (String object : this.updatedEntities) {
				response.add(object);
			}
		}
		return response;
	}
	/**
	 * Entidades actualizadas to set
	 * Set a copy
	 * @param updatedEntities
	 */
	public void setUpdatedEntities(List<String> updatedEntities) {
		List<String> response = new ArrayList<String>();
		if (updatedEntities != null && !updatedEntities.isEmpty()) {
			for (String object : updatedEntities) {
				response.add(object);
			}
		}
		this.updatedEntities = response;
	}
	/**
	 * Entidades reubicadas to get
	 * Return a copy
	 * @return
	 */
	public List<String> getMovedEntities() {
		List<String> response = new ArrayList<String>();
		if (this.movedEntities != null && !this.movedEntities.isEmpty()) {
			for (String object : this.movedEntities) {
				response.add(object);
			}
		}
		return response;
	}
	/**
	 * Entidades reubicadas to set
	 * Set a copy
	 * @param movedEntities
	 */
	public void setMovedEntities(List<String> movedEntities) {
		List<String> response = new ArrayList<String>();
		if (movedEntities != null && !movedEntities.isEmpty()) {
			for (String object : movedEntities) {
				response.add(object);
			}
		}
		this.movedEntities = response;
	}
	/**
	 * Entidades con excepciones to get
	 * Return a copy
	 * @return
	 */
	public List<String> getExceptionEntities() {
		List<String> response = new ArrayList<String>();
		if (this.exceptionEntities != null && !this.exceptionEntities.isEmpty()) {
			for (String object : this.exceptionEntities) {
				response.add(object);
			}
		}
		return response;
	}
	/**
	 * Entidades con excepciones to set
	 * Set a copy
	 * @param exceptionEntities
	 */
	public void setExceptionEntities(List<String> exceptionEntities) {
		List<String> response = new ArrayList<String>();
		if (exceptionEntities != null && !exceptionEntities.isEmpty()) {
			for (String object : exceptionEntities) {
				response.add(object);
			}
		}
		this.exceptionEntities = response;
	}
	/**
	 * Result to String
	 */
	@Override
	public String getResultToJSONString() {
		return null;
	}
}