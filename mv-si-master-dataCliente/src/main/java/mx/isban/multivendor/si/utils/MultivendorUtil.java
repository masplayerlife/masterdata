package mx.isban.multivendor.si.utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.multivendor.si.general.constantes.MultivendorConstantes;
import mx.isban.multivendor.si.masterdata.beans.AtmCajeroBean;


/** 
*   Isban Mexico
*   Clase: MultivendorUtil.java
*   Descripcion:
*   Clase utilitaria
*   Control de Cambios:
*   1.0 30/05/2018 JMAS - Creacion
*/
public final class MultivendorUtil extends Architech {
	
	/**
	 * ID
i8	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Static DESCRIPTION
	 */
	private static final String DESCRIPTION = "description";
	
	/**
	 * Atributo para setear instancia singleton
	 */
	private static MultivendorUtil util; 
	
	/**
	 * Crear solo una instancia a la clase
	 * @return 
	 */
	public static MultivendorUtil getInstance(){
		if (util == null) {
			util = new MultivendorUtil();
		}
		return util;
	}
	/**
	 * Función que transformar un boolean a su valor en string
	 * @param attr
	 * @return
	 */
	public String cambiarBooleanAString(boolean attr) {
		//valor boolean a regresar en String
		String attrString = "false";
		//Si es verdadero se cambia attrString a "true"
		//para regresar valor tipo string
		if (attr) {
			attrString = "true";
		}
		return attrString;
	}
	/**
	 * CREAR DATACENTER
	 * @param doc
	 * @param rootElement
	 */
	public void crearDataCenter(Document doc, Element rootElement) {
		this.info("CREANDO JERARQUIA DATACENTER");
		Element datacenter = doc.createElement("datacenter");
		rootElement.appendChild(datacenter);    		
		// DATACENTER name element
		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode(MultivendorConstantes.DATACENTER_NAME));
		datacenter.appendChild(name);
		// DATACENTER description element
		Element description = doc.createElement(DESCRIPTION);
		description.appendChild(doc.createTextNode(MultivendorConstantes.DATACENTER_DESCRIPTION));
		datacenter.appendChild(description);
		//DATACENTER address element
		Element addressListDtc = doc.createElement("address");    		
		datacenter.appendChild(addressListDtc);
		//street element
		Element street = doc.createElement("street");
		street.appendChild(doc.createTextNode(MultivendorConstantes.DATACENTER_STREET));
		addressListDtc.appendChild(street);    		
		//city element
		Element city = doc.createElement("city");
		city.appendChild(doc.createTextNode(MultivendorConstantes.DATACENTER_CITY));
		addressListDtc.appendChild(city);
	}
	/**
	 * CREAR MANDATOR
	 * @param doc
	 * @param rootElement
	 */
	public void crearMandator(Document doc, Element rootElement) {
		this.info("CREANDO JERARQUIA MANDATOR");
		Element mandator = doc.createElement("mandator");
		rootElement.appendChild(mandator);    		
		//MANDATOR name element
		Element nameMandator = doc.createElement("name");
		nameMandator.appendChild(doc.createTextNode(MultivendorConstantes.MANDATOR_NAME));
		mandator.appendChild(nameMandator);
		//MANDATOR description element
		Element descriptionMandator = doc.createElement(DESCRIPTION);
		descriptionMandator.appendChild(doc.createTextNode(MultivendorConstantes.MANDATOR_DESCRIPTION));
		mandator.appendChild(descriptionMandator);
	}
	/**
	 * CREAR INSTITUTE
	 * @param doc
	 * @param rootElement
	 */
	public void crearInstitute(Document doc, Element rootElement) {
		this.info("CREANDO JERARQUIA INSTITUTE");
		Element institute = doc.createElement("institute");
		rootElement.appendChild(institute);
		//INSTITUTE name element
		Element nameInstitute = doc.createElement("name");
		nameInstitute.appendChild(doc.createTextNode(MultivendorConstantes.INSTITUTE_NAME));
		institute.appendChild(nameInstitute);
		//INSTITUTE descripcion element
		Element descriptionInstitute = doc.createElement(DESCRIPTION);
		descriptionInstitute.appendChild(doc.createTextNode(MultivendorConstantes.INSTITUTE_DESCRIPTION));
		institute.appendChild(descriptionInstitute);
		//INSTITUTE routingcode element
		Element routingcode = doc.createElement("routingcode");    		
		institute.appendChild(routingcode);
		//routingcode element
		Element routingcodeElement = doc.createElement("routingcode");
		routingcodeElement.appendChild(doc.createTextNode(MultivendorConstantes.INSTITUTE_ROUTING_CODE));
		routingcode.appendChild(routingcodeElement);    		
		//shortroutingcode element
		Element shortroutingcode = doc.createElement("shortroutingcode");
		shortroutingcode.appendChild(doc.createTextNode(MultivendorConstantes.INSTITUTE_SHORT_ROUTING_CODE));
		routingcode.appendChild(shortroutingcode);
	}
	/**
	 * CREAR SUBSIDIARY
	 * @param doc
	 * @param rootElement
	 */
	public void crearSubsidiary(Document doc, Element rootElement) {
		this.info("CREANDO JERARQUIA SUBSIDIARY-BRANCH");
		Element subsidiary = doc.createElement("subsidiary");
		rootElement.appendChild(subsidiary);
		//INSTITUTE name element
		Element nameSubsidiary = doc.createElement("name");
		nameSubsidiary.appendChild(doc.createTextNode(MultivendorConstantes.SUBSIDIARY_NAME));
		subsidiary.appendChild(nameSubsidiary);
		//INSTITUTE description element
		Element descriptionSubsidiary = doc.createElement(DESCRIPTION);
		descriptionSubsidiary.appendChild(doc.createTextNode(MultivendorConstantes.SUBSIDIARY_DESCRIPTION));
		subsidiary.appendChild(descriptionSubsidiary);
	}
	/**
	 * CREAR WORKSTATIONS
	 * @param doc
	 * @param rootElement
	 * @param atmCaje
	 */
	public void crearWorkStation(Document doc, Element rootElement, AtmCajeroBean atmCaje) {
		this.info("CREANDO JERARQUIA WORKSTATION");
		Element workstation = doc.createElement("workstation");
		rootElement.appendChild(workstation);
		//WORKSTATION name element
		Element nameWorkstation = doc.createElement("name");
		nameWorkstation.appendChild(doc.createTextNode(atmCaje.getNumCajero()));
		workstation.appendChild(nameWorkstation);
		//WORKSTATION active element
		Element activeWorkstation = doc.createElement("active");
		activeWorkstation.appendChild(doc.createTextNode(atmCaje.getEstatus()));
		workstation.appendChild(activeWorkstation);
		//WORKSTATION street element
		Element streetWorkstation = doc.createElement("street");
		streetWorkstation.appendChild(doc.createTextNode(atmCaje.getCalle()));
		workstation.appendChild(streetWorkstation);
		//WORKSTATION zip code element
		Element zipCodeNoWorkstation = doc.createElement("zipcode");
		zipCodeNoWorkstation.appendChild(doc.createTextNode(atmCaje.getCp()));
		workstation.appendChild(zipCodeNoWorkstation);
		//WORKSTATION city element
		Element cityNoWorkstation = doc.createElement("city");
		cityNoWorkstation.appendChild(doc.createTextNode(atmCaje.getCiudad()));
		workstation.appendChild(cityNoWorkstation);
		//WORKSTATION description element
		Element descriptionWorkstation = doc.createElement(DESCRIPTION);
		descriptionWorkstation.appendChild(doc.createTextNode(atmCaje.getObservaciones()));
		workstation.appendChild(descriptionWorkstation);
	}	
	/**
	 * Metodo generico para crear atributos a los tags de XML
	 * @param doc
	 * @param attr
	 * @param valor
	 * @param agregarAElement
	 */
	public void crearAtributoXML(Document doc, String attr, String valor, Element agregarAElement) {
		Attr atributo = doc.createAttribute(attr);
		atributo.setValue(valor);
		agregarAElement.setAttributeNode(atributo);
	}
	/**
	 * Método que obtiene la propiedad requerida del archivo properties 
	 * @param property
	 * @return 
	 */
	public String getFieldProperties(String property) {
		Properties properties = getProperties(this.getConfigDeCmpAplicacion(
				MultivendorConstantes.PROP_RUTA_PROPERTIES_ERRORES));
		return properties.getProperty(property);
	}
	/**
	 * Método que obtiene el archivo de propiedades.
	 * @param rutaProperties
	 * @return
	 */
	public Properties getProperties(String rutaProperties) {
		Properties properties = new Properties();
		try (InputStream input = new FileInputStream(rutaProperties)){
			properties.load(input);
		} catch (IOException e) {
			this.error(e.getMessage(), e);
		}
		return properties;
	}
}
