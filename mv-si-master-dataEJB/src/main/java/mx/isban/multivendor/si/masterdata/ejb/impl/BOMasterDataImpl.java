package mx.isban.multivendor.si.masterdata.ejb.impl;

import java.io.StringWriter;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.BusinessException;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.multivendor.si.general.constantes.MultivendorConstantes;
import mx.isban.multivendor.si.masterdata.beans.AtmCajeroBean;
import mx.isban.multivendor.si.masterdata.beans.RequestMasterDataDTO;
import mx.isban.multivendor.si.masterdata.beans.ResponseMasterDataDTO;
import mx.isban.multivendor.si.masterdata.dao.DAOMasterData;
import mx.isban.multivendor.si.masterdata.ejb.BOMasterData;
import mx.isban.multivendor.si.utils.MultivendorUtil;
/**
 * Queretaro, Mayo 2018 Clase BOMasterDataImpl
 * 
 * Clase que contiene funcionalidad
 * de creación de archivo XML para ser enviado al servicio de Diebold
 * 
 * @author JMAS
 *
 */
@Stateless
@Remote(BOMasterData.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class BOMasterDataImpl extends Architech implements BOMasterData {
	/**
	 * ID.
	 */
	private static final long serialVersionUID = 8684134625139232637L;	
	/**
	 *logica de negocio
	 */
	@EJB
	private transient DAOMasterData daoMasterData;

	@Override
	public ResponseMasterDataDTO importarMasterData(RequestMasterDataDTO request, ArchitechSessionBean asb) throws BusinessException {
		this.info("ENTRANDO A IMPORTAR MASTERDATA");
		//Objeto de Respuesta
		ResponseMasterDataDTO resp = new ResponseMasterDataDTO();
		//Objeto de AtmCajeroBean para crear XML con data del cajero 
		AtmCajeroBean atmToXML;		
		try {
			//Obtiene el bean creado
			atmToXML = daoMasterData.obtenerDataCajero(request.getNumCajero(), asb);
			//Crear el XML
			String xmlImport = crearXML(atmToXML, request, asb);
			//Obtener respuesta de servicio de diebold
			resp = daoMasterData.enviarXML(MultivendorConstantes.USER, MultivendorConstantes.PASSWORD, xmlImport, asb);
		} catch (ExceptionDataAccess e) {
			//log de error
			this.error(e.getMessage(), e);
			throw new BusinessException(e.getCode(), e.getMessage());
		}
		this.info("SALIENDO DE importarMasterData");
		return resp;
	}

	@Override
	public String crearXML(AtmCajeroBean atmCaje, RequestMasterDataDTO request, ArchitechSessionBean asb) throws BusinessException {
		this.info("EMPIEZA CREAR XML");
		//Para guardar en variable el XML como String
		String xmlString = "";
		   try {
			   //Lib DocumentBuilderFactory 
	        	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	    		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	    		
	    		// root element
	    		Document doc = docBuilder.newDocument();
	    		Element rootElement = doc.createElement("pcemasterdata");
	    		doc.appendChild(rootElement);
	    		
	    		// set atributo xmlns, para pcemasterdata element
	    		MultivendorUtil.getInstance().crearAtributoXML(doc, "xmlns", "http://importexport.admin.myproclassic.com", rootElement);	    		
	    		// set atributo version, para pcemasterdata element
	    		MultivendorUtil.getInstance().crearAtributoXML(doc, "version", "3", rootElement);
	    		//CREAR TAG IMPORT MODE
	    		Element importXML = doc.createElement("import");
	    		rootElement.appendChild(importXML);	    		
	    		//set atributo createEntities, para import element
	    		MultivendorUtil.getInstance().crearAtributoXML(doc, "createEntities", MultivendorUtil.getInstance().cambiarBooleanAString(request.isCreateEntities()), importXML);
	    		//set atributo updateEntities, para import element
	    		MultivendorUtil.getInstance().crearAtributoXML(doc, "updateEntities", MultivendorUtil.getInstance().cambiarBooleanAString(request.isUpdateEntities()), importXML);	    		
	    		//set atributo deleteEntities, para import element
	    		MultivendorUtil.getInstance().crearAtributoXML(doc, "deleteEntities", "false", importXML);
	    		
	    		// CREAR JERARQUIA DATACENTER
	    		MultivendorUtil.getInstance().crearDataCenter(doc, rootElement);	    		
	    		// CREAR JERARQUIA MANDATOR
	    		MultivendorUtil.getInstance().crearMandator(doc, rootElement);	    		
	    		// CREAR JERARQUIA INSTITUTE
	    		MultivendorUtil.getInstance().crearInstitute(doc, rootElement);
	    		// CREAR JERARQUIA SUBSIDIARY
	    		MultivendorUtil.getInstance().crearSubsidiary(doc, rootElement);	    		
	    		// CREAR JERARQUIA WORKSTATION
	    		MultivendorUtil.getInstance().crearWorkStation(doc, rootElement, atmCaje);
	    		
	    		// Escribir el contenido en xml file
	    		TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    		Transformer transformer = transformerFactory.newTransformer();
	    		//Dar mejor formato de XML
	    		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
	    		DOMSource source = new DOMSource(doc);
	    		StringWriter writer = new StringWriter();
	    		StreamResult resultString = new StreamResult(writer);
	    		    		
	    		//Como String
	    		transformer.transform(source, resultString);
	    		//Se asigna a xmlString
	    		xmlString = writer.toString();
	    		//LOG DE RESULT XML
	    		this.info("LOG STRING XML...");
	    		this.info(xmlString);
	    		//Logear respuesta
	    		this.info("SE CREO XML MASTERDATA");

	        } catch (TransformerException ex) {
	        	//showException
	        	showException(ex);
	        	//Log error
	            this.error("Error outputting document " + ex.getMessage());
	            throw new BusinessException(ex.getMessage());
	        } catch (ParserConfigurationException ex) {
	        	//showException
	        	showException(ex);
	        	//Log error
	            this.error("Error building document " + ex.getMessage());
	            throw new BusinessException(ex.getMessage());
	        }
		   this.info("SALIENDO DE CREAR XML");
		   return xmlString;
	}
}
