package mx.isban.multivendor.si.masterdata.dao;

import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.multivendor.si.masterdata.beans.AtmCajeroBean;
import mx.isban.multivendor.si.masterdata.beans.ResponseMasterDataDTO;

/**
 * Queretaro, Mayo 2018
 * Interface DAOMasterData
 * 
 * Interfaz tipo DAO
 *  
 * @author JMAS
 * @version 1.0
 *
 */
public interface DAOMasterData {	
	/**
	 * Define metodo obtener data y crear XML
	 * @param numCajero
	 * @param asb
	 * @return
	 * @throws ExceptionDataAccess
	 */
	AtmCajeroBean obtenerDataCajero(String numCajero, ArchitechSessionBean asb) throws ExceptionDataAccess;
	/**
	 * Define metodo enviar XML a servicio de Diebold
	 * @param user
	 * @param password
	 * @param xml
	 * @param asb
	 * @return
	 */
	ResponseMasterDataDTO enviarXML(String user, String password, String xml, ArchitechSessionBean asb);
}