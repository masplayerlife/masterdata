package mx.isban.multivendor.si.constantes;

/**
 * Querétaro, Mayo 2018
 * 
 * Clase ConstantesQueries
 * 
 * Clase que contiene los queries que son utilizados por los servicios web
 * 
 * @author 
 * @version
 */
public final class ConstantesQueries {	
	/**
	 * Query para traer todos los datos del cajero
	 */
	public static final String DATA_CAJERO = "SELECT * FROM ATM_CAJEROS WHERE NUM_CAJERO = ?";
	/**
	 * Constructor
	 */
	private ConstantesQueries(){
		//default constructor
	}
/**
 * Fin de la clase
 */
}

