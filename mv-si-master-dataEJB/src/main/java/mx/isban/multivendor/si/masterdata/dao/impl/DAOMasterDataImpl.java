package mx.isban.multivendor.si.masterdata.dao.impl;


import java.util.HashMap;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.agave.commons.beans.ArchitechSessionBean;
import mx.isban.agave.commons.beans.LoggingBean;
import mx.isban.agave.commons.exception.ExceptionDataAccess;
import mx.isban.agave.dataaccess.DataAccess;
import mx.isban.agave.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import mx.isban.agave.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import mx.isban.agave.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import mx.isban.multivendor.si.constantes.ConstantesQueries;
import mx.isban.multivendor.si.general.constantes.MultivendorConstantes;
import mx.isban.multivendor.si.masterdata.beans.AtmCajeroBean;
import mx.isban.multivendor.si.masterdata.beans.ResponseMasterDataDTO;
import mx.isban.multivendor.si.masterdata.dao.DAOMasterData;

/**
 * Queretaro, Mayo 2018
 * DAOMasterDataImpl
 * 
 * Implementacion de DAOMasterData
 *  
 * @author JMAS
 * @version 1.0
 *
 */
@Stateless
@Local(DAOMasterData.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOMasterDataImpl extends Architech implements DAOMasterData {

	/**
	 * ID.
	 */
	private static final long serialVersionUID = 5391819775777687419L;
	/**
	 * Canal BD.
	 */
	private static final String ID_CANAL = "CANAL_DB_DATASOURCE";

	@Override
	public AtmCajeroBean obtenerDataCajero(String numCajero, ArchitechSessionBean asb) throws ExceptionDataAccess {
		this.info("OBTENER DATA CAJERO");
		//Objeto de respuesta
		AtmCajeroBean atm = new AtmCajeroBean();
		RequestMessageDataBaseDTO reqDTO = new RequestMessageDataBaseDTO();
		//Código operación SQL
		reqDTO.setCodeOperation("OBTENER_DATA_CAJERO");
		reqDTO.setTypeOperation(ConfigFactoryJDBC.OPERATION_TYPE_QUERY_PARAMS);
		reqDTO.setQuery(ConstantesQueries.DATA_CAJERO);
		reqDTO.addParamToSql(numCajero);
		//Objecto de respuesta
		ResponseMessageDataBaseDTO response = executeIDA(reqDTO);
		
		if (!ConfigFactoryJDBC.CODE_SUCCESFULLY.equals(response.getCodeError())) {
			//Excepción tipo DataAccess.
			throw new ExceptionDataAccess(response.getClass().getName(), response.getCodeError(),
					response.getMessageError());
		} else {
			//Si no viene vacia la respuesta de BD
			if (!response.getResultQuery().isEmpty()) {
				this.info("VIENE RESPUESTA DEL QUERY");
				//metod auxiliar para crear bean
				crearAtmCajeroBean(atm, response);
			} else {
				//Log no hay resultados				
				this.info("NO HUBO RESULTADO EN EL QUERY: " + response.getClass().getName() +" CODE: "
						+ response.getCodeError() +" MSG: "+ response.getMessageError());
				// No hay resultado se manda excepcion
				throw new ExceptionDataAccess(response.getClass().getName(), MultivendorConstantes.COD_ERR_NOT_FOUND,
						MultivendorConstantes.MSG_ERR_NOT_FOUND);
			}
		}
		//se regresa Bean creado
		return atm;		
	}

	@Override
	public ResponseMasterDataDTO enviarXML(String user, String password, String xml, ArchitechSessionBean asb) {
		this.info("ENTRANDO A ENVIAR XML");
		//será implementado para enviar XML a servicio de Diebold.
		ResponseMasterDataDTO response = new ResponseMasterDataDTO();
		response.setCodeError(MultivendorConstantes.COD_ERR_EXITO);
		response.setMessageError(MultivendorConstantes.MSG_ERR_EXITO);
		return response;
	}
	/**
	 * executeIDA
	 * @param request
	 * @return
	 * @throws ExceptionDataAccess
	 */
	private ResponseMessageDataBaseDTO executeIDA(RequestMessageDataBaseDTO request) throws ExceptionDataAccess {
		ResponseMessageDataBaseDTO responseDB;
		// Se prepara el objeto que recibirá los atributos de respuesta.
		DataAccess ida = DataAccess.getInstance(request, new LoggingBean());
		responseDB = (ResponseMessageDataBaseDTO) ida.execute(ID_CANAL);
		return responseDB;
	}
	/**	
	 * Auxiliar para crear Bean AtmCajeroBean de respuesta
	 * @param atm
	 * @param resQuery
	 * @return
	 */
	public AtmCajeroBean crearAtmCajeroBean(AtmCajeroBean atm, ResponseMessageDataBaseDTO resQuery) {
		//recorrer respuesta
		for (HashMap<String, Object> map : resQuery.getResultQuery()) {
			//setear Bean AtmCajeroBean 
			atm.setNumCajero(map.get("NUM_CAJERO").toString());
			atm.setNombre(map.get("NOMBRE").toString());
			atm.setFolio(map.get("FOLIO").toString());
			atm.setCalle(map.get("CALLE").toString());
			atm.setNum(map.get("NUM").toString());
			atm.setColonia(map.get("COLONIA").toString());
			atm.setDelegacion(map.get("DELEG_MUNIC").toString());
			atm.setCiudad(map.get("CIUDAD").toString());
			atm.setEstado(map.get("ESTADO").toString());
			atm.setCp(map.get("CP").toString());
			atm.setNumSerie(map.get("NUM_SERIE").toString());
			atm.setModelo(map.get("MODELO").toString());
			atm.setMarca(map.get("MARCA").toString());
			atm.setSerie(map.get("SERIE").toString());
			atm.setEstatus(map.get("ESTATUS").toString());
			atm.setObservaciones(map.get("OBSERVACIONES").toString());
		}
		//Log String de Bean AtmCajeroBean creado
		this.info("BEAN CREADO PARA XML = " + atm.toString());
		//regresa Bean
		return atm;
	}
}
